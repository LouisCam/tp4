using System;
using Xunit;
using MesozoicConsole;

namespace MesezoicTest
{
    public class UnitTest1
    {
       [Fact]
public void TestDinosaurConstructor()
{
    Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

    Assert.Equal("Louis", louis.name);
    Assert.Equal("Stegausaurus", louis.specie);
    Assert.Equal(12, louis.age);
}

[Fact]
public void TestDinosaurRoar()
{
    Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
    Assert.Equal("Grrr", louis.roar());
}

[Fact]
public void TestDinosaurSayHello()
{
    Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
    Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
}
    }
}
